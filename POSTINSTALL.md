The default username:password is admin:changeme!

To start using it, navigate to /admin/

Apparently this application doesn't have account management, this means if you want to change the password (which you definitely should!), open /app/data/config.php and just set a new plaintext password, which will be automatically encrypted.
As explained here: https://github.com/YOURLS/YOURLS/wiki/Username-Passwords

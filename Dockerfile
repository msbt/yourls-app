FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

ENV YVERSION=1.7.4

EXPOSE 8000

RUN apt-get update && apt-get install -y php-fpm php-cli php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath

RUN mkdir -p /app/code/ /app/data

WORKDIR /app/code

# install yourls & link configs
RUN wget https://github.com/YOURLS/YOURLS/archive/${YVERSION}.zip && \
    unzip ${YVERSION}.zip && \
    rm ${YVERSION}.zip && mv YOURLS-${YVERSION}/* /app/code && rm -rf YOURLS-${YVERSION} && \
    ln -s /app/data/config.php /app/code/user/config.php && \
    #ln -s /app/data/index.php /app/code/index.php && \
    ln -s /app/data/.htaccess /app/code/.htaccess


# configure apache
RUN rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    a2disconf other-vhosts-access-log

# configure mod_php
RUN a2enmod php7.2 rewrite
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 128M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/piwigo/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

ADD start.sh /app/
COPY index.html /app/code
ADD yourls.conf /etc/apache2/sites-enabled/yourls.conf

RUN chown -R www-data.www-data /app/data/ /app/code /run/

RUN echo "Listen 8000" > /etc/apache2/ports.conf

CMD [ "/app/start.sh" ]

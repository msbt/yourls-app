#!/bin/bash

set -eux

mkdir -p /run/nginx/log /run/nginx/lib

if [[ ! -d /app/data/config.php ]]; then
    echo "=> Detected first run"
	mkdir -p /app/data/
	# copy config
	cp /app/code/user/config-sample.php /app/data/config.php
	#cp /app/code/sample-public-front-page.txt /app/data/index.php

	# update config file
	sed -i "s/YOURLS_DB_HOST.*/YOURLS_DB_HOST', \'${MYSQL_HOST}\' );/" /app/data/config.php
	sed -i "s/YOURLS_DB_USER.*/YOURLS_DB_USER', \'${MYSQL_USERNAME}' );/" /app/data/config.php
	sed -i "s/YOURLS_DB_PASS.*/YOURLS_DB_PASS', \'${MYSQL_PASSWORD}' );/" /app/data/config.php
	sed -i "s/YOURLS_DB_NAME.*/YOURLS_DB_NAME', \'${MYSQL_DATABASE}' );/" /app/data/config.php

	sed -i "s/YOURLS_SITE.*/YOURLS_SITE', \'https:\/\/${APP_DOMAIN}' );/" /app/data/config.php
	sed -i "s/YOURLS_HOURS_OFFSET.*/YOURLS_HOURS_OFFSET', \'+1' );/" /app/data/config.php
	sed -i "s/'username'.*/'admin' => 'changeme!'/" /app/data/config.php
fi


# update mysql information
sed -i "s/YOURLS_DB_HOST.*/YOURLS_DB_HOST', \'${MYSQL_HOST}\' );/" /app/data/config.php
sed -i "s/YOURLS_DB_USER.*/YOURLS_DB_USER', \'${MYSQL_USERNAME}' );/" /app/data/config.php
sed -i "s/YOURLS_DB_PASS.*/YOURLS_DB_PASS', \'${MYSQL_PASSWORD}' );/" /app/data/config.php
sed -i "s/YOURLS_DB_NAME.*/YOURLS_DB_NAME', \'${MYSQL_DATABASE}' );/" /app/data/config.php


chown -R www-data.www-data /app/data /run/

echo "=> Run yourls"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
